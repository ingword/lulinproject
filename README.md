# README #

Using this example I’ll try to tell about tests and changes in the architecture of the application towards “testability”.

Let me put it straight – the project I mentioned does not have any “own” architecture, except for the one set by VCL library.

“Simple buttons”, simple “logic on form”.

That’s not bad for “small on-the-knee projects”. However, at some point, it makes our life harder, and so we have to “change something”.

But! STRAIGHT AWAY rushing for changes seems unreasonable to me.

To begin with, one should try to cover at least some of features by at least minimal testing.

I.e. FIRST “at least some tests”, only THEN – “changing of architecture”.

I’ll write about testing of this application in the [Testing of calculator. Table of contents](http://18delphi.blogspot.com/2015/03/testing-of-calculator-table-of-contents.html)